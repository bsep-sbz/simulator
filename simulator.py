import random
import sys
import syslog
import time

class State(object):
    def __init__(self):
        self.title = None

    def __str__(self):
        if self.title == 'request log':
            return 'Request log'
        if self.title == 'error log':
            return 'Error has occurred'
        if self.title == 'authentication success':
            return 'User successfully logged in'
        if self.title == 'authentication failure':
            return 'User failed to log in'
        if self.title == 'antivirus threat':
            return 'Antivirus threat has been detected'

def simulate(numbers, seconds):
    state = State()
    while True:
        n = random.randint(1, 1000)
        if n <= numbers[0]:
            state.title = 'request log'
            syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)
            syslog.syslog(syslog.LOG_INFO, str(state))
        elif n <= numbers[1]:
            state.title = 'error log'
            syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)
            syslog.syslog(syslog.LOG_ERR, str(state))
        elif n <= numbers[2]:
            state.title = 'authentication success'
            syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)
            syslog.syslog(syslog.LOG_INFO, str(state))
        elif n <= numbers[3]:
            state.title = 'authentication failure'
            syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)
            syslog.syslog(syslog.LOG_ERR, str(state))
        else:
            state.title = 'antivirus threat'
            syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_SYSLOG)
            syslog.syslog(syslog.LOG_WARNING, str(state))
        syslog.closelog()
        time.sleep(seconds)

def main():
    mode = sys.argv[1]
    
    seconds = 0
    numbers = []
    if mode == 'normal':
        seconds = 2
        numbers = [600, 800, 900, 950]
    elif mode == 'attack':
        seconds = 0.5
        numbers = [200, 500, 550, 850]

    simulate(numbers, seconds)

if __name__ == "__main__":
        main()